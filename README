A python3-based HTTP 1.1 server for ingesting and serving DASH media streams.

Alpha quality, use at your own risk.

To run, simply execute the shipped dash_server.py file with a Python 3
interpreter. No dependencies beyond the standard library are neded. The
executable takes one mandatory parameter, which is the path to the directory to
which the DASH media files shall be written.

The server processes incoming HTTP GET, PUT, POST and DELETE requests. PUT and
POST are treated identically - request body is written into a file inside the
media directory, with the name equal to decoded request target. For requests
using chunked transfer encoding, the body shall also be temporarily cached in
memory and will be available to incoming GET requests while it is being
received. To GET requests the server will serve either the accordingly-named
file from the media directory or the aforementioned cache entry.
DELETE requests make the server remove accordingly-named files in the media
directory if they are not currently being uploaded.

The server is written to be as simple as possible and is intended to be deployed
as a backend behind a gateway HTTP server such as nginx (sample config is
provided in nginx_config). The gateway would typically handle client
authentication, serving the non-DASH and finalized DASH media files.
